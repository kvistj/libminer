
<!-- README.md is generated from README.Rmd. Please edit that file -->

# libminer

<!-- badges: start -->
<!-- badges: end -->

The goal of libminer is to provide an overview of your R library. It is
a toy packages from a workshop and not meant for a serious use.

## Installation

You can install the development version of libminer like so:

``` r
devtools::install_gitlab("kvistj/libminer)
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(libminer)

lib_summary()
#>                                                                   Library
#> 1                                      C:/Program Files/R/R-4.3.1/library
#> 2                       C:/Users/kvij1477/AppData/Local/R/win-library/4.3
#> 3 C:/Users/kvij1477/AppData/Local/Temp/RtmpimNvXK/temp_libpath48f442e34b9
#>   n_packages
#> 1         30
#> 2        341
#> 3          1
```
